package com.beans;

public class JobDetails {
	
	private int job_id;
	private int companyCode , min_experience;
	private String designation;
	private String min_qualification;
	private  String location;
	private  String jobDescription;
	
	public JobDetails(int job_id, int companyCode, int min_experience, String designation, String min_qualification,
			String location, String jobDescription) {
		
		this.job_id = job_id;
		this.companyCode = companyCode;
		this.min_experience = min_experience;
		this.designation = designation;
		this.min_qualification = min_qualification;
		this.location = location;
		this.jobDescription = jobDescription;
	}
	
	
	public int getJob_id() {
		return job_id;
	}
	public void setJob_id(int job_id) {
		this.job_id = job_id;
	}
	public int getCompanyCode() {
		return companyCode;
	}
	public void setCompanyCode(int companyCode) {
		this.companyCode = companyCode;
	}
	public int getMin_experience() {
		return min_experience;
	}
	public void setMin_experience(int min_experience) {
		this.min_experience = min_experience;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getMin_qualification() {
		return min_qualification;
	}
	public void setMin_qualification(String min_qualification) {
		this.min_qualification = min_qualification;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getJobDescription() {
		return jobDescription;
	}
	public void setJobDescription(String jobDescription) {
		this.jobDescription = jobDescription;
	}
	
	
	
	
	
	
	

}
