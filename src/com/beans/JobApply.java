package com.beans;

public class JobApply {
	 
	private int  userId;
	private int jobId;
	private boolean isSelected;
	public JobApply(int userId, int jobId) {
		
		this.userId = userId;
		this.jobId = jobId;
		
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getJobId() {
		return jobId;
	}
	public void setJobId(int jobId) {
		this.jobId = jobId;
	}
	public boolean isSelected() {
		return isSelected;
	}
	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}
	
	
	
}
