package com.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.beans.CompanyDetails;
import com.dao.CompanyDetailsImpl;

@Controller
@RequestMapping("/register")
public class RegisterController {

	@RequestMapping(method = { RequestMethod.POST, RequestMethod.GET })
	public String getDetailsToRegister(HttpServletRequest request, ModelMap modelMap) {
		try {
			int companyCode = Integer.parseInt(request.getParameter("companyCode"));
			String companyName = request.getParameter("companyName");
			String domain = request.getParameter("domain");
			String companyProfile = request.getParameter("companyProfile");
			int year = Integer.parseInt(request.getParameter("year"));
			String email = request.getParameter("email");
			long contact = Long.parseLong(request.getParameter("contact"));
			String password = request.getParameter("password");

			CompanyDetails companyDetails = new CompanyDetails();
			companyDetails.setCompanyCode(companyCode);
			companyDetails.setCompanyName(companyName);
			companyDetails.setCompanyProfile(companyProfile);
			companyDetails.setContact(contact);
			companyDetails.setDomain(domain);
			companyDetails.setEmail(email);
			companyDetails.setYear(year);
			companyDetails.setPassword(password);

			// call the DAO Method
			ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
			CompanyDetailsImpl impl = (CompanyDetailsImpl) context.getBean("companyDetailsImpl");
			int status = impl.registerCompany(companyDetails);

			if (status == 1) {
				return "login";
			} else {
				return "index";
			}
		} catch (Exception e) {
			modelMap.addAttribute("error", "Error occured during processing..");
			return "index";
		}
	}
}
