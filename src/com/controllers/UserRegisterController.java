package com.controllers;

import java.sql.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.beans.CompanyDetails;
import com.beans.UserDetails;
import com.dao.CompanyDetailsImpl;
import com.dao.UserDetailsImpl;

@Controller
@RequestMapping("/userregister")
public class UserRegisterController {

	@RequestMapping(method = { RequestMethod.POST, RequestMethod.GET })
	public String getDetailsToRegisterOfUser(HttpServletRequest request, ModelMap modelMap) {

		String userName = request.getParameter("userName");
		long mobile = Long.parseLong(request.getParameter("mobile"));
		String email = request.getParameter("email");
		String degree = request.getParameter("degree");
		String specialization = request.getParameter("specialization");
		Integer isFresher = Integer.parseInt(request.getParameter("isFresher"));
		String yearsOfExperience = request.getParameter("yearsOfExperience");
		String aboutMe = request.getParameter("aboutMe");
		String password = request.getParameter("password");
		String dobb = request.getParameter("dob");
		Date dob = Date.valueOf(dobb);

		UserDetails userDetails = new UserDetails();
		userDetails.setUserName(userName);
		userDetails.setMobile(mobile);
		userDetails.setAboutMe(aboutMe);
		userDetails.setDegree(degree);
		userDetails.setDob(dob);
		userDetails.setEmail(email);
		userDetails.setIsFresher(isFresher);
		userDetails.setPassword(password);
		userDetails.setSpecialization(specialization);
		userDetails.setYearsOfExperience(yearsOfExperience);

		// call the DAO Method
		ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
		UserDetailsImpl impl = (UserDetailsImpl) context.getBean("USerDetailsImpl");
		int status = impl.registerUser(userDetails);

		if (status == 1) {
			return "login";
		} else {
			return "index";
		}
	}
}
