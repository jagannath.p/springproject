package com.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.beans.JobDetails;
import com.dao.CompanyDetailsImpl;
import com.dao.JobDetailsImpl;

@Controller
@RequestMapping("/postJob")
public class JobDetailsController {
	
	@RequestMapping(method = {RequestMethod.POST, RequestMethod.GET})
	public String getJobDetails(HttpServletRequest request, ModelMap modelMap) {
		
		try {
			int companyCode = Integer.parseInt(request.getParameter("companyCode"));
			int job_id = Integer.parseInt(request.getParameter("jobId"));
			String designation = request.getParameter("designation");
			String minimumQualification  = request.getParameter("");
			String location = request.getParameter("location");
			int minimumExperience =Integer.parseInt(request.getParameter("minimumExperience"));
			String jobDescription = request.getParameter("jobDescription");
			
			JobDetails job = new JobDetails(job_id, companyCode, minimumExperience, designation, minimumQualification, location, jobDescription)
			
					// call the Dao Operations 
			ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
			JobDetailsImpl impl = (JobDetailsImpl) context.getBean("JobDetailsImpl");
			int status = impl.registerJob(job);
			
			if (status == 1) {
				//Return True 
				return " ";
			} else {
				
				//Return false 
				return " ";
			}
					
		}catch(Exception e) {
			return null;
		}
		
		
		
		
	}
	
	

}
