package com.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomePageController {
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String hetHomeJsp() {
		return "home";
	}

	@RequestMapping(value = "/Uregister", method = RequestMethod.GET)
	public String getUserRegistrationJsp() {
		return "UserRegistration";
	}

	@RequestMapping(value = "/companyregister", method = RequestMethod.GET)
	public String getCompanyRegistrationJsp() {
		return "index";
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String getLoginJsp() {
		return "login";
	}
	
	@RequestMapping(value = "/companyDashboard", method = RequestMethod.GET)
	public String getCompanyDashboard() {
		return "companyDashboard";
	}
	
	@RequestMapping(value = "/companyAddJob", method = RequestMethod.GET)
	public String getCompanyAddJob() {
		return "companyAddJob";
	}
	
	@RequestMapping(value = "/companyViewJobs", method = RequestMethod.GET)
	public String getCompanyViewJobs() {
		return "companyViewJobs";
	}
	
	@RequestMapping(value = "/companyViewCandidates", method = RequestMethod.GET)
	public String getCompanyViewCandidates() {
		return "companyViewCandidates";
	}
}
