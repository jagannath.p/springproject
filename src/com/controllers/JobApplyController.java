package com.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.beans.JobApply;
import com.dao.JobApplyImpl;
import com.dao.JobDetailsImpl;

@Controller
@RequestMapping("/applyjob")
public class JobApplyController {
	
	public String applyJob(HttpServletRequest request, ModelMap modelMap) {
		
		
		/* public JobApply(int userId, int jobId, boolean isSelected) */
		
		int jobId = Integer.parseInt(request.getParameter("jobId"));
		int userId = Integer.parseInt(request.getParameter("userId"));
		
		
		JobApply  application  = new JobApply(userId,jobId);
		
		ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
		JobApplyImpl impl = (JobApplyImpl) context.getBean("JobApplyImpl");
		int status = impl.registerApplication(application);
		
		if (status == 1) {
			
			//Return True 
			return " ";
		} else {
			
			//Return false 
			return " ";
		}
	}
	
	

}
