package com.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.dao.CompanyDetailsImpl;
import com.dao.UserDetailsImpl;

@Controller
@RequestMapping("/loginValidation")
public class LoginValidationController {

	@RequestMapping(method = { RequestMethod.POST, RequestMethod.GET })
	public String checkIfUserValidated(HttpServletRequest request, ModelMap modelMap) {

		String email = request.getParameter("email");
		String password = request.getParameter("password");
		String type = request.getParameter("loginType");

		int status;
		System.out.println("Email;: " + email);
		System.out.println("Email;: " + password);
		System.out.println("Email;: " + type);
		switch (type) {
		
		case "1": { // User Login
			
			ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
			UserDetailsImpl userDetailsImpl = (UserDetailsImpl) context.getBean("USerDetailsImpl");
			status = userDetailsImpl.checkIfUserValidated(email, password);
			if (status > 0) {
				// Login Success
				modelMap.addAttribute("type", "jobSeeker");
				return "dashboard";
			}
			break;
		}
		case "2": { // Company Login

			ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
			CompanyDetailsImpl companyDetailsImpl = (CompanyDetailsImpl) context.getBean("companyDetailsImpl");
			status = companyDetailsImpl.checkIfUserValidated(email, password);
			System.out.println("status = "+ status);
			if (status > 0 ) {
				// Login Success
				modelMap.addAttribute("type", "company");
				return "dashboard";
			}
			break;
		}
		}

		modelMap.addAttribute("status", "Login Failed");
		return "login";
	}

}
