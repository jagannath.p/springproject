package com.dao;

import com.beans.CompanyDetails;

public interface CompanyDetailsDao {

	public Integer registerCompany(CompanyDetails details);
	
	public Integer checkIfUserValidated(String email, String password);
}
