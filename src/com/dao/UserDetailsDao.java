package com.dao;

import com.beans.UserDetails;

public interface UserDetailsDao {

	public Integer registerUser(UserDetails userDetails);
	
	public Integer checkIfUserValidated(String email, String password);
}
