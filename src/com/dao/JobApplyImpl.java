package com.dao;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.beans.JobApply;

public class JobApplyImpl implements JobApplyDao {
	
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;
	
	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public Integer registerApplication(JobApply details) {
		
		if (details == null) {
			return -1;
		}
		String insertQuery = "INSERT INTO  usersapplied VALUES(?,?)";
		int status = jdbcTemplate.update(insertQuery, 	details.getUserId(), 
														details.getJobId());

		if (status > 0) {
			return 1;
		} else {
			return 0;
		}
	}

}
