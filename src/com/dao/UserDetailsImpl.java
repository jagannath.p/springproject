package com.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.beans.UserDetails;

public class UserDetailsImpl implements UserDetailsDao {

	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public Integer registerUser(UserDetails userDetails) {
		if (userDetails == null) {
			return -1;
		}
		String insertQuery = "INSERT INTO userDetails(name, mobile, email, dob, degree, specialization, isFresher, yearsOfExperience, password, aboutMe) "
				+ "VALUES(?,?,?,?,?,?,?,?,?,?)";
		int status = jdbcTemplate.update(insertQuery, userDetails.getUserName(), userDetails.getMobile(),
				userDetails.getEmail(), userDetails.getDob(), userDetails.getDegree(), userDetails.getSpecialization(),
				userDetails.getIsFresher(), userDetails.getYearsOfExperience(), userDetails.getPassword(),
				userDetails.getAboutMe());

		if (status > 0) {
			return 1;
		} else {
			return 0;
		}
	}

	@Override
	public Integer checkIfUserValidated(String email, String password) {
		String validateQuery = "SELECT * FROM userDetails WHERE email = '" + email + "' and password ='" + password
				+ "'";
		int id = 0;
		id = jdbcTemplate.query(validateQuery, new ResultSetExtractor<Integer>() {

			@Override
			public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {

				if (rs.next()) {
					int id = rs.getInt(1);		
					System.out.println("ID: " + id);
					return id;
				}
				return -1;
			}
		});
		return id;
	}

}
