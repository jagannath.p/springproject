package com.dao;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.beans.JobDetails;

public class JobDetailsImpl  implements JobDetailsDao{

	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;
		public DataSource getDataSource() {
			return dataSource;
		}

		public void setDataSource(DataSource dataSource) {
			this.dataSource = dataSource;
			this.jdbcTemplate = new JdbcTemplate(dataSource);
		}

		/*  public JobDetails(int job_id, int companyCode, int min_experience, String designation, String min_qualification,
			String location, String jobDescription)   */
		@Override
		public Integer registerJob(JobDetails details) {
			if (details == null) {
				return -1;
			}
			String insertQuery = "INSERT INTO job_details VALUES(?,?,?,?,?,?,?)";
			int status = jdbcTemplate.update(insertQuery,details.getJob_id(),details.getCompanyCode(),details.getMin_experience(),details.getDesignation()
					, details.getMin_qualification(),details.getLocation(),details.getJobDescription());

			if (status > 0) {
				return 1;
			} else {
				return 0;
			}
			
		}
	
		
		
		
		
		
	

}
