package com.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.beans.CompanyDetails;

public class CompanyDetailsImpl implements CompanyDetailsDao {

	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public Integer registerCompany(CompanyDetails details) {
		if (details == null) {
			return -1;
		}
		String insertQuery = "INSERT INTO companyDetails VALUES(?,?,?,?,?,?,?,?)";
		int status = jdbcTemplate.update(insertQuery, 	details.getCompanyCode(), 
														details.getCompanyName(),
														details.getDomain(), 
														details.getCompanyProfile(), 
														details.getYear(), 
														details.getEmail(),
														details.getContact(), 
														details.getPassword());

		if (status > 0) {
			return 1;
		} else {
			return 0;
		}
	}

	@Override
	public Integer checkIfUserValidated(String email, String password) {
		String validateQuery = "SELECT * FROM companyDetails WHERE email = '" + email + "' and password ='" + password
				+ "'";
		int id = 0;
		id = jdbcTemplate.query(validateQuery, new ResultSetExtractor<Integer>() {

			@Override
			public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {

				if (rs.next()) {
					int id = rs.getInt(1);		
					System.out.println("ID: " + id);
					return id;
				}
				return -1;
			}
		});
		return id;
	}
}
