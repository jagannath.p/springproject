package com.dao;


import com.beans.JobDetails;

public interface JobDetailsDao {

	public Integer registerJob(JobDetails details);
	
}
