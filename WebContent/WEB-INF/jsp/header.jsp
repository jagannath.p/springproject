<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang=''>
<head>

<spring:url value="/resources/styles.css" var="stylesCSS" />
<spring:url value="/resources/css/jobportal.css" var="portalCSS" />
<spring:url value="/resources/css/bootstrap.min.css" var="bootstrapCSS" />
<script src="http://code.jquery.com/jquery-latest.min.js"
	type="text/javascript"></script>
<script src="script.js"></script>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<link href="${stylesCSS}" rel="stylesheet" />
<link href="${bootstrapCSS}" rel="stylesheet" />
<link href="${portalCSS}" rel="stylesheet" />


<meta charset='utf-8'>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>CSS MenuMaker</title>
</head>
<body>
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark nav-specific">
	  <!-- Brand/logo -->
	  <a class="navbar-brand brand-specific" href="./">Job<span>Portal</span></a>
	  
	  <!-- Links -->
	  <ul class="navbar-nav">
	    <li class="nav-item">
	      <a class="nav-link" href="#">Companies</a>
	    </li>
	    <li class="nav-item">
	      <a class="nav-link" href="#">Jobs</a>
	    </li>
	    <li class="nav-item dropdown">
	      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Register</a>
	      <div class="dropdown-menu dropdown-specific" aria-labelledby="navbardrop">
	        <a class="dropdown-item" href="./Uregister">Job Seeker</a>
	        <a class="dropdown-item" href="./companyregister">Company</a>
	      </div>
	    </li>
	    <li class="nav-item">
	      <a class="nav-link" href="./login">Login</a>
	    </li>
	  </ul>
	</nav>
	
</body>
<html>