<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Job Portal</title>
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>
	
	<div class="jumbotron">
		<h1 class="text-center">Job Seeker Registration</h1>
		<div class="row">
			<div class="col-lg-6 offset-lg-3">
				<form action="userregister" method="post">
					<div class="form-group">
					    <label for="userName">Name:</label>
					    <input type="text" class="form-control" id="userName" name="userName"  placeholder="Enter Name"
								required>
					</div>
					<div class="form-group">
					    <label for="mobile">Mobile:</label>
					    <input type="text" class="form-control" id="mobile" pattern="[0-9]+" name="mobile"
								placeholder="Enter Mobile number" required>
					</div>
					<div class="form-group">
					    <label for="email">Email:</label>
					    <input type="email" class="form-control" id="email" name="email" placeholder="Enter Email"
								required>
					</div>
					<div class="form-group">
					    <label for="dob">Date Of Birth:</label>
					    <input type="date" class="form-control" id="dob" name="dob"  pattern = "[0-9][0-9][0-9][0-9]-[01][0-9]-[0123][0-9]"
								placeholder="format(YYYY-MM-DD)" required>
					</div>
					<div class="form-group">
					    <label for="degree">Degree:</label>
					    <input type="text" class="form-control" id="degree" name="degree"  placeholder="Enter Degree(BE, ME, ..)" required>
					</div>
					<div class="form-group">
					    <label for="specialization">Specialization:</label>
					    <input type="text" class="form-control" id="specialization" name="specialization"  placeholder="Enter Specialization course" required>
					</div>
					<div class="form-group">
				      	<label for="isFresher">Fresher:</label>
				      	<div class="row">
				      		<div class="col-lg-6">
				      			<input type="radio" id="isFresher1" name="isFresher" value="1" checked>Yes
				      		</div>
				      		<div class="col-lg-6">
				      			<input type="radio" id="isFresher2" name="isFresher" value="0">No
				      		</div>	
				      	</div>
				    </div>
					<div class="form-group">
					    <label for="yearsOfExperience">Years of Experience:</label>
					    <input type="text" class="form-control" id="yearsOfExperience" name="yearsOfExperience"  pattern="[0-9]+" placeholder="Enter Years of Experience" required>
					</div>
					<div class="form-group">
					    <label for="yearsOfExperience">About Me:</label>
					    <textarea class="form-control" id="aboutMe" name="aboutMe"  placeholder="Description"
								value="" rows="5"></textarea>
					</div>
					<div class="form-group">
					    <label for="password">Password:</label>
					    <input type="password" class="form-control" id="password" name="password"  placeholder="Enter Password" required>
					</div>
					<button type="submit" class="btn btn-primary offset-lg-5">Register</button>
				</form>		
			</div>
		</div>
	
		
	</div>
	</body>
</html>