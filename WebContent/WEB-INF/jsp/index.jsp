<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Job Portal</title>
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>
	<div class="jumbotron">
		<h1 class="text-center">Company Registration</h1>
		<div class="row">
			<div class="col-lg-6 offset-lg-3">
				<form action="register" method="post">
					<div class="form-group">
					    <label for="companyCode">Company Code:</label>
					    <input type="text" class="form-control" id="companyCode" pattern="[0-9]+" name="companyCode"
					placeholder="Enter Company Code" required>
					</div>
					<div class="form-group">
					    <label for="companyName">Company Name:</label>
					    <input type="text" class="form-control" id="companyName" name="companyName"
					placeholder="Enter Company Name" required>
					</div>
					<div class="form-group">
					    <label for="domain">Domain:</label>
					    <input type="text" class="form-control" id="domain" name="domain" placeholder="Enter Domain"
					required>
					</div>
					<div class="form-group">
					    <label for="dob">Company Profile:</label>
					    <input type="text" class="form-control" id="companyProfile" name="companyProfile"
					placeholder="Enter Company's Profile" required>
					</div>
					<div class="form-group">
					    <label for="year">Year Founded:</label>
					    <input type="text" class="form-control" id="year" name="year"
					placeholder="Enter Year Founded(YYYY)" required>
					</div>
					<div class="form-group">
					    <label for="email">Company's Email:</label>
					    <input type="email" class="form-control" id="email" name="email" placeholder="Enter Email ID"
								required>
					</div>
					<div class="form-group">
					    <label for="contact">Contact Number:</label>
					    <input type="text" class="form-control" id="contact" pattern="[0-9]+" name="contact"
								placeholder="Enter Contact Number" required>
					</div>
					<div class="form-group">
					    <label for="password">Password:</label>
					    <input type="password" class="form-control" id="password" name="password"  placeholder="Enter Password" required>
					</div>
					<button type="submit" class="btn btn-primary offset-lg-5">Register</button>
				</form>		
			</div>
		</div>
	
		
	</div>
	
	<!-- <h2>Registration Form</h2>
	<form action="register" method="post">
		<table>
			<tr>
				<td><label>Password: </label></td>
				<td><input type="password" name="password"
					placeholder="Enter Password" required></td>
			</tr>
			<tr>
				<td><input type="submit" value="Register"></td>
			</tr>
		</table>
	</form> -->
</body>
</html>