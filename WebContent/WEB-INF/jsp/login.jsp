<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Job Portal</title>
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>

	<div class="jumbotron">
		<h1 class="text-center">Login</h1>
		<div class="row">
			<div class="col-lg-4 offset-lg-4">
				<form action="loginValidation" method="post">
					<div class="form-group">
						<label for="email">Email:</label> <input type="email"
							class="form-control" id="email" name="email"
							placeholder="Enter Email" required>
					</div>
					<div class="form-group">
						<label for="password">Password:</label> <input type="password"
							class="form-control" id="password" name="password"
							placeholder="Enter Password" required>
					</div>
					<div class="form-group">
						<label for="loginType">Login Type:</label> 
						<select	class="form-control" name="loginType" id="loginType">
							<option value="1">Job Seeker</option>
							<option value="2">Company</option>
						</select>
					</div>
					<button type="submit" class="btn btn-primary offset-lg-5">Login</button>
				</form>
			</div>
		</div>
	</div>
	<div>
		<%
		try
		{
			String status = "";
			status = request.getParameter("status");
			if (status.equals("Login Failed")) {
		%>
		${status}
		<%
			}
		}catch(Exception ex)
		{
			
		}
		%>
	</div>
</body>
</html>