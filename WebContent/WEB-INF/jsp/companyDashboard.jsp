<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>
	<div class="jumbotron">
		<h1 class="text-center">Xoriant Solutions | Dashboard</h1>
	</div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-2">
				<div class="list-group">
					<a href="" class="list-group-item list-group-item-action list-group-item-dark active">Dashboard Home</a>
					<a href="./companyAddJob" class="list-group-item list-group-item-action list-group-item-dark">Add Job Opening</a>
					<a href="./companyViewJobs" class="list-group-item list-group-item-action list-group-item-dark">View Job Openings</a>
					<a href="./companyViewCandidates" class="list-group-item list-group-item-action list-group-item-dark">View Candidates</a>
				</div>
			</div>
		</div>
	</div>
</body>
</html>